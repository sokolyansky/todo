import callWebApi from "../helpers/webApiHelper";

export const getAllProjects = async (filter) => {
    const response = await callWebApi({
        endpoint: "/api/projects",
        type: "GET",
        query: filter
    });
    return response.json();
};

export const getProject = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/projects/${id}`,
        type: "GET"
    });
    return response.json();
};

export const addProject = async (request) => {
    const response = await callWebApi({
        endpoint: "/api/projects",
        type: "POST",
        request
    });
    return response.json();
};

export const updateProject = async (request) => {
    const response = await callWebApi({
        endpoint: "/api/projects",
        type: "PUT",
        request
    });
    return response.json();
};

export const deleteProject = async (request) => {
    const response = await callWebApi({
        endpoint: "/api/projects",
        type: "DELETE",
        request
    });
    return response.json();
};
