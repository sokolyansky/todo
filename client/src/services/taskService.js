import callWebApi from "../helpers/webApiHelper";

export const handleTask = async (request) => {
    if (request.id) {
        return request.isDeleting ?
            await deleteTask(request)
            :
            await updateTask(request);
    }
    return await addTask(request);
};

export const getTask = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/tasks/${id}`,
        type: "GET"
    });
    return response.json();
};

export const addTask = async (request) => {
    const response = await callWebApi({
        endpoint: "/api/tasks",
        type: "POST",
        request
    });
    return response.json();
};

export const updateTask = async (request) => {
    const response = await callWebApi({
        endpoint: "/api/tasks",
        type: "PUT",
        request
    });
    return response.json();
};

export const deleteTask = async (request) => {
    const response = await callWebApi({
        endpoint: "/api/tasks",
        type: "DELETE",
        request
    });
    return response.json();
};
