import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Modal, Header, Form, Grid, Input, Button } from "semantic-ui-react";
import { addProject } from "../Todo/actions";

class ProjectModal extends React.Component {
    state = {
        open: true,
        name: this.props.name
    };

    closeModal = () => {
        this.props.finishEditing();
    };

    handleAddItem = () => {
        const { name } = this.state;
        this.props.finishEditing(name);
    };

    handleChange = (ev) => {
        this.setState({ name: ev.target.value })
    };

    render() {
        const { name } = this.state;
        const { header, placeholder } = this.props;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                <Modal.Content>
                    <Form reply onSubmit={this.handleAddItem}>
                        <Header as="h3" dividing>
                            { header }
                        </Header>
                        <Input
                            fluid
                            placeholder={placeholder}
                            value={name}
                            onChange={ev => this.handleChange(ev)}
                        />
                        <Grid padded>
                            <Grid.Column textAlign="center">
                                <Button type="button" content="Cancel" negative onClick={this.closeModal} />
                                <Button type="submit" content="Save" labelPosition="left" icon="edit" primary />
                            </Grid.Column>
                        </Grid>

                    </Form>

                </Modal.Content>

            </Modal>
        );
    }
}

ProjectModal.propTypes = {
    finishCreating: PropTypes.func,
    currentUserId: PropTypes.string
};

const actions = {
    addProject
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(ProjectModal);
