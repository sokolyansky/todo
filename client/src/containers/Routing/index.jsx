import React from "react";
import { Route, Switch } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Todo from "../Todo";
import Login from "../../components/Login";
import Registration from "../../components/Registration";
import Profile from "../Profile";
import Header from "../../components/Header";
import Spinner from "../../components/Spinner";
import NotFound from "../../scenes/NotFound";
import PrivateRoute from "../PrivateRoute";
import Notifications from "../../components/Notifications";
import { loadCurrentUser, logout, login, registration } from "../Profile/actions";

class Routing extends React.Component {
    componentDidMount() {
        this.props.loadCurrentUser();
    }

    renderLogin = loginProps => (
        <Login
            {...loginProps}
            isAuthorized={this.props.isAuthorized}
            login={this.props.login}
        />
    );

    renderRegistration = regProps => (
        <Registration
            {...regProps}
            isAuthorized={this.props.isAuthorized}
            registration={this.props.registration}
        />
    );

    render() {
        const { isLoading, isAuthorized, user, ...props } = this.props;
        return (
            isLoading
                ? <Spinner />
                : (
                    <div className="fill">
                        {isAuthorized && (
                            <header>
                                <Header user={user} logout={props.logout} />
                            </header>
                        )}
                        <main className="fill">
                            <Switch>
                                <Route exact path="/login" render={this.renderLogin} />
                                <Route exact path="/registration" render={this.renderRegistration} />
                                <PrivateRoute exact path="/" component={Todo} />
                                <PrivateRoute exact path="/profile" component={Profile} />
                                <Route path="*" exact component={NotFound} />
                            </Switch>
                        </main>
                        <Notifications  user={user} />
                    </div>
                )
        );
    }
}

Routing.propTypes = {
    isAuthorized: PropTypes.bool,
    logout: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    registration: PropTypes.func.isRequired,
    user: PropTypes.objectOf(PropTypes.any),
    isLoading: PropTypes.bool,
    loadCurrentUser: PropTypes.func.isRequired,
    userId: PropTypes.string,
};

Routing.defaultProps = {
    isAuthorized: false,
    user: {},
    isLoading: true,
    userId: undefined
};

const actions = { loadCurrentUser, login, logout, registration };

const mapStateToProps = rootState => ({
    isAuthorized: rootState.profile.isAuthorized,
    user: rootState.profile.user,
    isLoading: rootState.profile.isLoading,
    userId: rootState.profile.user && rootState.profile.user.id
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Routing);
