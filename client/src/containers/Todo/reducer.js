import {
    LOAD_MORE_PROJECTS,
    SET_ALL_PROJECTS,
    ADD_PROJECT,
    EDIT_PROJECT,
    DELETE_PROJECT
} from "./actionTypes";

export default (state = {}, action) => {
    let projects;

    switch (action.type) {
        case SET_ALL_PROJECTS:
            return {
                ...state,
                projects: action.projects,
                hasMoreProjects: action.projects.length === action.limit
            };
        case LOAD_MORE_PROJECTS:
            return {
                ...state,
                projects: [...(state.projects || []), ...action.projects],
                hasMoreProjects: action.projects.length === action.limit
            };
        case ADD_PROJECT:
            return {
                ...state,
                projects: [action.project, ...state.projects]
            };
        case EDIT_PROJECT:
            projects = [...state.projects];
            projects = projects
                .map(project => project.id === action.project.id
                    ? {...project, ...action.project} : project);
            return {
                ...state,
                projects
            };
        case DELETE_PROJECT:
            projects = [...state.projects]
                .filter(project => project.id !== action.project.id);
            return {
                ...state,
                projects
            };

        default:
            return state;
    }
};
