import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import InfiniteScroll from "react-infinite-scroller";
import { Loader } from "semantic-ui-react";

import ProjectModal from "../ProjectModal";
import Project from "../../components/Project";
import {
    loadMoreProjects, addProject, editProject
} from "./actions";

import styles from "./styles.module.scss";

class Todo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCreating: false
        };
        this.postsFilter = {
            userId: undefined,
            from: 0,
            count: 10
        };
    }

    createProject = () => {
        this.setState(
            ({ showOwnPosts }) => ({ showOwnPosts: !showOwnPosts }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showOwnPosts ? this.props.userId : undefined,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter, this.incrementPageNumber);
            }
        );
    };

    startCreating = () => {
        this.setState(
            () => ({ isCreating: true })
        )
    };

    finishCreating = (name) => {
        name && this.props.addProject({name});
        this.setState(
            () => ({ isCreating: false })
        )
    };

    incrementPageNumber = () => {
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    };

    loadMoreProjects = () => {
        this.props.loadMoreProjects(this.postsFilter);
        this.incrementPageNumber();
    };

    render() {
        const { projects = [], hasMoreProjects, ...props } = this.props;
        const header= "Type your Project name";
        const placeholder= "Start typing here to create a project";
        return (
            <div className={styles.projectContent}>

                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMoreProjects}
                    hasMore={hasMoreProjects}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {projects.map(project => (
                        <Project
                            key={project.id}
                            project={project}
                            finishEditing={this.finishEditing}
                            currentUserId={props.userId}
                            deletePost={props.deletePost}
                        />
                    ))}
                </InfiniteScroll>
                <div className="ui vertical center aligned segment">
                    <button className="ui blue button" onClick={this.startCreating}>
                        Add TODO List
                    </button>
                </div>
                {
                    this.state.isCreating
                    && <ProjectModal
                        header={header}
                        placeholder={placeholder}
                        finishEditing={this.finishCreating} />
                }
            </div>
        );
    }
}

Todo.propTypes = {
    projects: PropTypes.arrayOf(PropTypes.object),
    hasMoreProjects: PropTypes.bool,
    userId: PropTypes.string,
    loadMoreProjects: PropTypes.func.isRequired,
    addProject: PropTypes.func.isRequired,
    editProject: PropTypes.func.isRequired
};

Todo.defaultProps = {
    projects: [],
    hasMoreProjects: true,
    userId: undefined
};

const mapStateToProps = rootState => ({
    projects: rootState.projects.projects,
    hasMoreProjects: rootState.projects.hasMoreProjects,
    userId: rootState.profile.user.id
});

const actions = {
    loadMoreProjects,
    addProject,
    editProject
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Todo);
