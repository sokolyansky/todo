import * as projectService from "../../services/projectService";
import * as taskService from "../../services/taskService";
import {
    LOAD_MORE_PROJECTS,
    SET_ALL_PROJECTS,
    ADD_PROJECT,
    EDIT_PROJECT,
    DELETE_PROJECT
} from "./actionTypes";

const addMoreProjectsAction = (projects, limit) => ({
    type: LOAD_MORE_PROJECTS,
    projects,
    limit
});

const setProjectsAction = (projects, limit) => ({
    type: SET_ALL_PROJECTS,
    projects,
    limit
});

const addProjectAction = project => ({
    type: ADD_PROJECT,
    project
});

const editProjectAction = project => ({
    type: EDIT_PROJECT,
    project
});

const deleteProjectAction = project => ({
    type: DELETE_PROJECT,
    project
});


export const loadMoreProjects = filter => async (dispatch) => {
    const projects = await projectService.getAllProjects(filter);
    dispatch(addMoreProjectsAction(projects, filter.count));
};

export const addProject = request => async (dispatch, getRootState) => {
    const { id } = await projectService.addProject(request);
    const newProject = await projectService.getProject(id);
    dispatch(addProjectAction(newProject));
};

export const editProject = project => async (dispatch) => {
    const updatedProject = await projectService.updateProject(project);
    dispatch(editProjectAction(updatedProject));
};

export const deleteProject = project => async (dispatch) => {
    const res = await projectService.deleteProject(project);
dispatch(deleteProjectAction({ id: project.id }));
};

export const handleTask = request => async (dispatch, getRootState) => {
    const { id } = await taskService.handleTask(request);
    const task = await taskService.getTask(id);
    const mapTasks = project => ({
        ...project,
        //taskCount: Number(project.commentCount) + 1,
        tasks: request.id ?
            project.tasks.map(existTask => existTask.id === task.id ? task : existTask)
            :
            [...(project.tasks || []), task] // task is taken from the current closure
    });


    const { projects: { projects } } = getRootState();
    const updated = projects.map(project => (project.id !== task.projectId
        ? project
        : mapTasks(project)));

    dispatch(setProjectsAction(updated));
};

export const deleteTask = request => async (dispatch, getRootState) => {
    await taskService.handleTask({...request, isDeleting: true});

    const mapTasks = project => ({
        ...project,
        tasks: project.tasks.filter(task => task.id !== request.id)
    });

    const { projects: { projects } } = getRootState();
    const updated = projects.map(project => (project.id !== request.projectId
        ? project
        : mapTasks(project)));

    dispatch(setProjectsAction(updated));
};
