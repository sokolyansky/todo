import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers
} from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
// import { composeWithDevTools } from "redux-devtools-extension";
import logger from "./middlewares/logger";

import projectReducer from "./containers/Todo/reducer";
import profileReducer from "./containers/Profile/reducer";

export const history = createBrowserHistory();

const initialState = {};

const middlewares = [
    thunk,
    routerMiddleware(history), logger
];

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const reducers = {
    projects: projectReducer,
    profile: profileReducer
};

const rootReducer = combineReducers({
    router: connectRouter(history),
    ...reducers
});

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);
window.store = store;
export default store;
