export default store => next => (action) => {
    console.log(">>>>11", "dispatching", action);
    console.log(">>>>22", "state before", store.getState());
    next(action);
    console.log(">>>>33", "state after", store.getState());
}
