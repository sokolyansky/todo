import React from "react";
import { Image, Header } from "semantic-ui-react";

import styles from "./styles.module.scss";

export const Logo = () => (
    <Header as="h2" color="grey" className={styles.logoWrapper}>
        <Image circular src="login.png" />
        {" "}
        Ruby Garage
    </Header>
);

export default Logo;
