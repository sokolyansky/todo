import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import { Card, Image, Icon, Input, Button, Form } from "semantic-ui-react";
import moment from "moment";

import { editProject, deleteProject, handleTask } from "../../containers/Todo/actions";
import ProjectModal from "../../containers/ProjectModal";
import Task from "../Task";


class Project extends Component {
    state = {
        isEditing: false,
        body: ""
    };

    finishEditing = (name) => {
        const { id, userId } = this.props.project;
        name && this.props.editProject({id, userId, name});
        this.setState({isEditing: false});
    };

    handleChangeTask = (ev) => {
        this.setState({ body: ev.target.value })
    };

    handleTask = () => {
        const { body: name } = this.state;
        if (!name) {
            return;
        }
        const { id: projectId } = this.props.project;
        this.props.handleTask({ projectId, name, status: false });
        this.setState({ body: "" });
    };

    render() {
        const { project, deleteProject } = this.props;
        const {
            id,
            image,
            name,
            user,
            userId,
            tasks,
            createdAt
        } = project;
        const date = moment(createdAt).fromNow();

        return (
            <div>
                <Card style={{width: "100%", marginBottom: 24}}>
                    {image && <Image src={image.link} wrapped ui={false}/>}
                    <Card.Content style={{backgroundColor: "#3366CC"}}>
                        <Card.Meta style={{color: "white"}}>
                            <h5 style={{display: "inline-block"}}>{name}</h5>
                            <span className="date">
                                    created by
                                {" "}
                                {user.username}
                                {" - "}
                                {date}
                                </span>
                            <Icon
                                link
                                size="big"
                                color="orange"
                                name="pencil"
                                onClick={() => this.setState({isEditing: true})}
                            />
                            <Icon
                                link
                                size="big"
                                color="orange"
                                name="trash"
                                onClick={() => deleteProject({id, userId})}
                            />
                        </Card.Meta>
                    </Card.Content>
                    <Card.Content extra>
                        <Form onSubmit={this.handleTask}>
                            <Input
                                fluid
                                action={
                                    <Button className="blue" type="submit">Add Task</Button>
                                }
                                placeholder="Start typing here to create a task..."
                                value={this.state.body}
                                onChange={ev => this.handleChangeTask(ev)}
                            />
                        </Form>
                    </Card.Content>
                    <Card.Content extra style={{padding: 0}}>
                        {tasks.map(task => (
                            <Task
                                key={task.id}
                                task={task}
                                projectId={id}
                                userId={userId}
                                deleteTask={this.handleTask}
                            />
                        ))}
                    </Card.Content>
                </Card>
                {
                    this.state.isEditing
                    && <ProjectModal finishEditing={this.finishEditing} name={name} />
                }
            </div>
        );
    }
}


Project.propTypes = {
    currentUserId: PropTypes.string,
    project: PropTypes.objectOf(PropTypes.any).isRequired,
    deletePost: PropTypes.func
};
Project.defaultProps = {
    currentUserId: null,
    deletePost: null,
};

const actions = {
    editProject, deleteProject, handleTask
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(Project);
