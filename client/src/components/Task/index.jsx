import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import { Grid, Icon } from "semantic-ui-react";
import ProjectModal from "../../containers/ProjectModal";
import { handleTask, deleteTask } from "../../containers/Todo/actions";


class Task extends Component {
    state = {
        status: this.props.task.status,
        isEditing: false,
        isAction: false
    };

    finishEditing = (name) => {
        const { projectId, userId, task: { id } } = this.props;
        name && this.props.handleTask({id, projectId, userId, name});
        this.setState({isEditing: false});
    };

    handleChangeStatus = ({target: {checked: status}}) => {
        const { projectId, userId, task: { id } } = this.props;
        this.props.handleTask({id, projectId, userId, status});
        this.setState({status});
    };

    deleteTask = () => {
        const { task: {id}, userId, projectId } = this.props;
        this.props.deleteTask({ id, userId, projectId })
    };

    render() {
        const { status, isAction } = this.state;
        const {
            task: { name }
        } = this.props;
        const header = "Edit your Task name";

        return (
            <Grid celled style={{margin: 0}}>
                <Grid.Row
                    color={isAction ? "yellow" : null}
                    onMouseEnter={() => this.setState({isAction: true})}
                    onMouseLeave={() => this.setState({isAction: false})}
                >
                    <Grid.Column width={2} textAlign="center">
                        <input type="checkbox" checked={status} onChange={this.handleChangeStatus}/>
                    </Grid.Column>
                    <Grid.Column width={11}>{name}</Grid.Column>
                    <Grid.Column width={3} textAlign="center">
                        {isAction &&
                            <div>
                                <Icon name="sort"/>
                                <Icon link name="pencil" onClick={() => this.setState({isEditing: true})} />
                                <Icon link name="trash" onClick={this.deleteTask} />
                            </div>
                        }
                    </Grid.Column>
                </Grid.Row>
                {
                    this.state.isEditing
                    && <ProjectModal
                        name={name}
                        header={header}
                        finishEditing={this.finishEditing}
                        />
                }
            </Grid>
        )
    }

}

const actions = {
    handleTask, deleteTask
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(Task);
