# mini-project ToDo RubyGarage

## Описание
[**ToDo**](git@github.com:) - это [SPA](https://medium.com/NeotericEU/single-page-application-vs-multiple-page-application-2591588efe58 "SPA")


## Технологии

Здесь перечислены основные фреймворки и библиотеки, используемые в проекте. Полный список используемых технологий для каждой части проекта находится в файлах package.json в папках client и server.

#### Common
1. ES2018
2. [Git](https://git-scm.com/book/ru/v1/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git "Git")
3. [REST API](https://www.restapitutorial.com/lessons/restquicktips.html "REST API")
4. [JWT](https://en.wikipedia.org/wiki/JSON_Web_Token "JWT")
5. [Socket.IO](https://socket.io/docs/ "Socket.IO")
6. [npm](https://en.wikipedia.org/wiki/Npm_(software))
7. [ESLint](https://eslint.org/docs/user-guide/getting-started "ESLint")

#### Frontend
1. [React](https://reactjs.org/docs/getting-started.html "React")
2. [React Redux](https://redux.js.org/introduction/getting-started "React Redux")
3. [React Semantic UI](https://react.semantic-ui.com/ "React Semantic UI")
4. [Moment.js](https://momentjs.com/ "Moment.js")
5. [validator.js](https://www.npmjs.com/package/validator "validator.js")
6. [history](https://www.npmjs.com/package/history "history")

#### Backend
1. [Node.js](https://nodejs.org/en/ "Node.js")
2. [Express](https://expressjs.com/ru/guide/routing.html "Express")
3. [Passport.js](http://www.passportjs.org/docs/ "Passport.js")
4. [Sequelize](http://docs.sequelizejs.com/ "Sequelize")
5. [axios](https://www.npmjs.com/package/axios "axios")
6. [bcrypt](https://www.npmjs.com/package/bcrypt "bcrypt")
7. [Babel](https://babeljs.io/docs/en/index.html "Babel")
8. [nodemon](https://www.npmjs.com/package/nodemon "nodemon")
9. [dotenv](https://www.npmjs.com/package/dotenv "dotenv")
10. [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken "jsonwebtoken")

#### Database
1. [PostgreSQL](https://www.postgresql.org/download/ "PostgreSQL")

## Установка

1. Установить последнюю стабильную версию [Node.js](https://nodejs.org/en/ "Node.js") (LTS).
    
2. Установить последнюю стабильную версию [PostgreSQL](https://www.postgresql.org/download/ "PostgreSQL") для вашей OS. Для работы с базой данных может использоваться клиент [pgAdmin](https://www.pgadmin.org/ "pgAdmin").

3. Создать в PostgreSQL **пустую** базу данных для проекта. Например, *toDo*.

#### Backend

1.  В папке server создать файл **.env**, скопировать в него содержимое из файла **.env.example** и заменить значения ключей на действительные.
	
2. Выполнить [миграции](http://docs.sequelizejs.com/manual/migrations.html#running-migrations "миграции"):

    ```
    npx sequelize-cli db:migrate
    ```
    
3. Для запуска сервера в командной строке (терминале) в папке сервера выполнить:

    ```
    npm start
    ```

#### Frontend

1.  В папке client создать файл **.env**, скопировать в него содержимое из файла **.env.example** и заменить значения ключей на действительные.
    
2. Для запуска клиента в командной строке (терминале) в папке клиента выполнить:

    ```
    npm start
    ``` 
