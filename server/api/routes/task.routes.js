import { Router } from "express";
import * as taskService from "../services/task.service";
import checkOwnerMiddleware from "../middlewares/check-owner.middleware";

const router = Router();

router
    .get("/:id", (req, res, next) => taskService.getTaskById(req.params.id)
        .then(task => res.send(task))
        .catch(next))
    .post("/", (req, res, next) => taskService.create(req.user.id, req.body)
        .then(task => res.send(task))
        .catch(next))
    .put("/", checkOwnerMiddleware, (req, res, next) => taskService.update(req.user.id, req.body)
        .then(task => res.send(task))
        .catch(next))
    .delete("/", checkOwnerMiddleware, (req, res, next) => taskService.deleteTask(req.body.id)
        .then(() => res.status(200).send({ text: "ok" }))
        .catch(next))
    .post("/get", (req, res, next) => taskService.getTasks(req.user.id, req.body)
        .then(task => res.send(task))
        .catch(next));

export default router;
