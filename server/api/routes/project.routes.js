import { Router } from "express";
import * as projectService from "../services/project.service";
import checkOwnerMiddleware from "../middlewares/check-owner.middleware";

const router = Router();

router
    .get("/", (req, res, next) => projectService.getProjects(req.user.id, req.query)
            .then(projects => res.send(projects))
            .catch(next))
    .get("/:id", (req, res, next) => projectService.getProjectById(req.params.id)
        .then(project => res.send(project))
        .catch(next))
    .post("/", (req, res, next) => projectService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((project) => {
            req.io.emit("new_project", project); // notify all users that a new project was created
            return res.send(project);
        })
        .catch(next))
    .put("/", checkOwnerMiddleware, (req, res, next) => projectService.update(req.user.id, req.body)
        .then(project => res.send(project))
        .catch(next))
    .delete("/", checkOwnerMiddleware, (req, res, next) => projectService.softDelete(req.body.id)
        .then(() => res.status(200).send({ text: "ok" }))
        .catch(next))

export default router;
