export default (req, res, next) => {
    if (req.user.id !== req.body.userId) {
        const err = new Error("Access denied!");
        err.status = 401;
        throw err;
    }
    next();
};
