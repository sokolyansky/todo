import projectRepository from "../../data/repositories/project.repository";

export const getProjects = (userId, filter) => projectRepository.getProjects({
    ...filter,
    userId
});

export const getProjectById = id => projectRepository.getProjectById(id);

export const create = (userId, project) => projectRepository.create({
    ...project,
    userId
});

export const update = (userId, project) => projectRepository.updateById({
    ...project,
    userId
});

export const softDelete = id => projectRepository.deleteById(id);

