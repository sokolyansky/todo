import taskRepository from "../../data/repositories/task.repository";

export const create = (userId, task) => taskRepository.create({
    ...task,
    userId
});

export const update = (userId, task) => taskRepository.updateById({
    ...task,
    userId
});

export const deleteTask = id => taskRepository.deleteById(id);

export const getTaskById = id => taskRepository.getTaskById(id);

export const getTasks = (userId, arr) => taskRepository.getTasks(arr);
