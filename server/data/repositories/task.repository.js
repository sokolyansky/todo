import sequelize from "../db/connection";
import { TaskModel, UserModel } from "../models/index";
import BaseRepository from "./base.repository";

const { Op } = sequelize.constructor.Sequelize;

class TaskRepository extends BaseRepository {
    getTaskById(id) {
        return this.model.findOne({
            where: { id },
            include: [{
                model: UserModel,
                attributes: ["id", "username"]
            }]
        });
    }
}

export default new TaskRepository(TaskModel);
