import sequelize from "../db/connection";
import { ProjectModel, TaskModel, UserModel } from "../models/index";
import BaseRepository from "./base.repository";

const { ne } = sequelize.constructor.Op;

class ProjectRepository extends BaseRepository {
    async getProjects(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            isInclude,
            likedByCurrentUser
        } = filter;

        return this.model.findAll({
            where: { userId },
            include: [
                {
                    model: UserModel,
                    attributes: ["id", "username"]
                },
                {
                    model: TaskModel,
                    attributes: ["id", "name", "status"]
                }
            ],
            order: [["createdAt", "DESC"]],
            offset,
            limit,
            //logging: (sql) => console.log(">>>>88", sql)
        });
    }

    getProjectById(id) {
        return this.model.findOne({
            where: { id },
            include: [{
                model: TaskModel,
                include: {
                    model: UserModel,
                    attributes: ["id", "username"]
                }
            }, {
                model: UserModel,
                attributes: ["id", "username"]
            }]
        });
    }
}

export default new ProjectRepository(ProjectModel);
