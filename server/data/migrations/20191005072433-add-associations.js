export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.addColumn("projects", "userId", {
                type: Sequelize.UUID,
                references: {
                    model: "users",
                    key: "id",
                },
                onUpdate: "CASCADE",
                onDelete: "SET NULL",
            }, { transaction }),
            queryInterface.addColumn("tasks", "projectId", {
                type: Sequelize.UUID,
                references: {
                    model: "projects",
                    key: "id",
                },
                onUpdate: "CASCADE",
                onDelete: "SET NULL",
            }, { transaction }),
            queryInterface.addColumn("tasks", "userId", {
                type: Sequelize.UUID,
                references: {
                    model: "users",
                    key: "id",
                },
                onUpdate: "CASCADE",
                onDelete: "SET NULL",
            }, { transaction })
        ])),

    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn("projects", "userId", { transaction }),
            queryInterface.removeColumn("tasks", "projectId", { transaction }),
            queryInterface.removeColumn("tasks", "userId", { transaction })
        ]))
};
