export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .query("CREATE EXTENSION IF NOT EXISTS pgcrypto;")
        .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
        //.transaction(transaction => Promise.all([
            queryInterface.addColumn("projects", "deletedAt", Sequelize.DATE, { transaction }),
            queryInterface.addColumn("tasks", "deletedAt", Sequelize.DATE, { transaction })
        ]))),

    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn("projects", "deletedAt", { transaction }),
            queryInterface.removeColumn("tasks", "deletedAt", { transaction })
        ]))
};
