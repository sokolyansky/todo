export default (orm, DataTypes) => {
    const Project = orm.define("project", {
        name: {
            allowNull: false,
            type: DataTypes.TEXT
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        deletedAt: DataTypes.DATE
    }, {
        timestamps: true,
        paranoid: true
    });

    return Project;
};
