export default (orm, DataTypes) => {
    const Task = orm.define("task", {
        name: {
            allowNull: false,
            type: DataTypes.TEXT
        },
        status: {
            allowNull: false,
            type: DataTypes.BOOLEAN
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        deletedAt: DataTypes.DATE
    }, {
        timestamps: true,
        paranoid: true
    });

    return Task;
};
