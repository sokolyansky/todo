/* eslint-disable no-console */
import Sequelize from "sequelize";
import * as config from "../../config/db.config";

const con = {...config, dialectOptions: {ssl: true}, dialect_options: {ssl: true}, sslmode: "require"};
//const sequelize = new Sequelize(process.env.DATABASE_URL, con);
const sequelize = new Sequelize(config);
console.log(">>>>22", con)
console.log(">>>>23", process.env.DATABASE_URL)
sequelize
    .authenticate()
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch((err) => {
        console.error("Unable to connect to the database:", err);
    });

export default sequelize;
