export default (models) => {
    const {
        User,
        Project,
        Task
    } = models;

    User.hasMany(Project);
    User.hasMany(Task);

    Project.belongsTo(User);
    Project.hasMany(Task);

    Task.belongsTo(User);
    Task.belongsTo(Project);
};
